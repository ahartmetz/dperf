#include <arguments.h>
#include <connectioninfo.h>
#include <eventdispatcher.h>
#include <message.h>
#include <pendingreply.h>
#include <transceiver.h>

#include <cassert>
#include <iostream>

int main(int argc, char *argv[])
{
    int bufSize = -1;

    if (argc >= 2) {
        bufSize = atoi(argv[1]);
    }

    const bool direct = argc >= 3 && argv[2] == std::string("direct");
    ConnectionInfo connection(direct ? ConnectionInfo::Bus::PeerToPeer : ConnectionInfo::Bus::Session);
    if (direct) {
        connection.setSocketType(ConnectionInfo::SocketType::AbstractUnix);
        connection.setRole(ConnectionInfo::Role::Client);
        connection.setPath("dperf-test-socket");
    }

    if (bufSize < 0) {
        std::cerr << "Usage : client <buf_size> [direct]\n"
                     "        If you pass direct, you must also pass it to the server.\n";
        exit(-1);
    }

    EventDispatcher dispatcher;
    Transceiver transceiver(&dispatcher, connection);

    const std::string payloadBuf(bufSize, 'A');
    const cstring payload(payloadBuf.c_str(), bufSize);
    const std::string expectedReply = "OK";

    for (int i = 0; i < 20000 ; i++) {
        Message msg = Message::createCall("/com/test/app", "com.test.app", "HelloTestApp");
        if (!direct) {
            msg.setDestination("com.test.app");
        }
        Arguments::Writer writer;
        writer.writeString(payload);
        msg.setArguments(writer.finish());

        PendingReply pendingReply = transceiver.send(std::move(msg));

        while (!pendingReply.isFinished()) {
            dispatcher.poll();
        }

        if (pendingReply.isError()) {
            std::cerr << "Sorry, something went wrong.\n";
            exit(-1);
        }

        Arguments::Reader reader(pendingReply.reply()->arguments());
        assert(reader.state() == Arguments::String);
        cstring str = reader.readString();
        if (str.ptr != expectedReply) {
            std::cerr << "Wrong answer " << str.ptr << " in reply message.\n";
            exit(-1);
        }
    }
}
