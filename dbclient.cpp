#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <gio/gio.h>

#define DBUS_NAME "com.test.app"
#define DBUS_PATH "/com/test/app"
#define DBUS_IFACE "com.test.app"

void method_call (GDBusConnection *conn, char *buf)
{
  GVariant *result;
  GError *error;
  int cnt;
  struct timespec start, end;
  double elapsed;

  error = NULL;
  clock_gettime (CLOCK_REALTIME, &start);

  for (cnt = 0; cnt < 20000 ; cnt++)
    {
      result = g_dbus_connection_call_sync (conn,
                                            DBUS_NAME,
                                            DBUS_PATH,
                                            DBUS_IFACE,
                                            "HelloTestApp",
                                            g_variant_new ("(s)",buf),
                                            G_VARIANT_TYPE ("(s)"),
                                            G_DBUS_CALL_FLAGS_NONE,
                                            -1,
                                            NULL,
                                            &error);
      if (result != NULL)
        {
          gchar *ret;

          g_variant_get (result, "(s)", &ret);
          g_variant_unref (result);

          g_assert_cmpstr (ret, ==, "OK");
          g_free (ret);
        }
      else
        {
          fprintf (stderr, "Sorry, something went wrong: %s\n", error->message);
          exit (EXIT_FAILURE);
        }
    }

  clock_gettime (CLOCK_REALTIME, &end);
  elapsed = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) / 1E9;
  printf ("elapsed time : %lf s\n", elapsed);
}

int main (int argc, char ** argv)
{
  GDBusConnection *conn = NULL;
  GError *err = NULL;

  int cnt, buf_size;
  char *buf = NULL;

  if (argc < 2)
    {
      fprintf (stderr, "Usage : client_gdbus <buf_size>\n");
      exit (EXIT_FAILURE);
    }

  conn = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &err);
  if (conn == NULL)
    {
      fprintf (stderr, "Error connecting to D-Bus bus: %s\n", err->message);
      g_error_free (err);
      exit (EXIT_FAILURE);
    }

  /* malloc buffer */
  buf_size = atoi (argv[1]);
  buf = (char*) malloc (sizeof(char) * (buf_size + 1));
  for (cnt = 0; cnt < buf_size; cnt++)
    buf[cnt] = 'A';
  buf[cnt] = '\0';

  method_call (conn, buf);

  /* cleanup */
  free (buf);
  return 0;
}
