#include <arguments.h>
#include <connectioninfo.h>
#include <error.h>
#include <eventdispatcher.h>
#include <imessagereceiver.h>
#include <message.h>
#include <transceiver.h>

#include <cassert>
#include <iostream>

class PingResponder : public IMessageReceiver
{
public:
    Transceiver *m_transceiver;

    virtual void spontaneousMessageReceived(Message message) override
    {
        // There is no DBus object tree model (yet) so let's use a lame and admittedly
        // unfairly faster substitute
        if (message.type() != Message::MethodCallMessage || message.method() != "HelloTestApp" ||
            message.interface() != "com.test.app") {
            return;
        }

        Message pong = Message::createReplyTo(message);
        Arguments::Writer writer;
        writer.writeString("OK");
        pong.setArguments(writer.finish());

        m_transceiver->sendNoReply(std::move(pong));
    }
};

int main(int argc, char *argv[])
{
    EventDispatcher dispatcher;

    const bool direct = argc >= 2 && argv[1] == std::string("direct");
    ConnectionInfo connection(direct ? ConnectionInfo::Bus::PeerToPeer : ConnectionInfo::Bus::Session);
    if (direct) {
        connection.setSocketType(ConnectionInfo::SocketType::AbstractUnix);
        connection.setRole(ConnectionInfo::Role::Server);
        connection.setPath("dperf-test-socket");
    }

    Transceiver transceiver(&dispatcher, connection);

    PingResponder responder;
    responder.m_transceiver = &transceiver;
    transceiver.setSpontaneousMessageReceiver(&responder);

    if (!direct) {
        // Register unique name on bus
        Message msg = Message::createCall("/org/freedesktop/DBus", "org.freedesktop.DBus", "RequestName");
        msg.setDestination("org.freedesktop.DBus");
        msg.setMethod("RequestName");

        Arguments::Writer writer;
        writer.writeString("com.test.app"); // requested name
        writer.writeUint32(4); // 4 == DBUS_NAME_FLAG_DO_NOT_QUEUE
        msg.setArguments(writer.finish());

        transceiver.sendNoReply(std::move(msg));
    }

    // send the name request and start waiting for incoming messages (to that name)

    while (dispatcher.poll()) {
        if (!transceiver.isConnected()) {
            std::cout << "broken connection, exiting.\n";
            break;
        }
    }
}
